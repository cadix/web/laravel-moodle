<?php

namespace Cadix\LaravelMoodle;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\ExpectedValues;

class Auth
{
    public function __construct(public Client $client)
    {
    }

    /**
     * Adds a new user (pending to be confirmed) in the site
     *
     * @param array $user
     * @return array|null
     * @throws GuzzleException|Exception\MoodleException
     */
    public function auth_email_signup_user(
        #[ArrayShape([
            'username'               => 'string',
            'password'               => 'string',
            'firstname'              => 'string',
            'lastname'               => 'string',
            'email'                  => 'string',
            'city'                   => 'string|null',
            'country'                => 'string|null',
            'recaptchachallengehash' => 'string|null',
            'recaptcharesponse '     => 'string|null',
            'customprofilefields '   => 'array|null',
            'redirect'               => 'string|null',
        ])]
        array $user
    ): array|null {
        $this->client->url = 'auth_email_signup_user';

        foreach ($user as $field => $value) {
            $this->client->url .= sprintf(
                '&%s=%s',
                $field,
                $value
            );
        }

        return $this->client->request();
    }

    public function create(
        #[ArrayShape([
            'username'               => 'string',
            'password'               => 'string',
            'firstname'              => 'string',
            'lastname'               => 'string',
            'email'                  => 'string',
            'city'                   => 'string|null',
            'country'                => 'string|null',
            'recaptchachallengehash' => 'string|null',
            'recaptcharesponse '     => 'string|null',
            'customprofilefields '   => 'array|null',
            'redirect'               => 'string|null',
        ])]
        array $user
    ): array|null {
        return $this->auth_email_signup_user($user);
    }

    /**
     * Requests a password reset
     *
     * @param string $field
     * @param string $value
     * @return bool
     * @throws GuzzleException|Exception\MoodleException
     */
    public function core_auth_request_password_reset(
        #[ExpectedValues([
            'email',
            'username',
        ])]
        string $field,
        string $value
    ): bool {
        $this->client->url = sprintf(
            'core_auth_request_password_reset&%s=%s',
            $field,
            $value
        );

        return ! in_array($this->client->request()[ 'status' ], ['emailpasswordconfirmnotsent', 'emailpasswordconfirmnoemail']);
    }

    /**
     * Resend confirmation email
     *
     * @param string $username
     * @param string|null $password
     * @param string|null $redirect_uri
     * @return array|null
     * @throws GuzzleException|Exception\MoodleException
     */
    public function core_auth_resend_confirmation_email(string $username, string|null $password = null, string|null $redirect_uri = null): array|null
    {
        $this->client->url = sprintf(
            'core_auth_resend_confirmation_email&username=%s&password=%s&redirect=%s',
            $username,
            $password ?? Str::random(12),
            $redirect_uri
        );

        return $this->client->request();
    }
}
