<?php

namespace Cadix\LaravelMoodle;

use Cadix\LaravelMoodle\Exception\MoodleException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Http;

/**
 * A wrapper class around the Moodle API
 */
class Client
{
    public string $url;
    public string $baseUrl;

    public function __construct()
    {
        $this->baseUrl = config('moodle.uri');
    }

    /**
     * Send the request
     *
     * @param array $body
     * @param int $tries
     * @return array|null
     * @throws GuzzleException|MoodleException
     */
    public function request(array $body = [], int $tries = 0): array|null
    {
        $url = $this->baseUrl.$this->url;
        $response = Http::timeout(config('moodle.timeout'))
            ->post($url, $body);

        if ($response->failed()) {
            if ($tries >= config('moodle.retries')) {
                throw new \Exception('More than ' . $tries . ' tries for request ' . $url . ' without success '.$response->status());
            }

            return $this->request($body, ++$tries);
        }

        $response = $response->json();

        if (isset($response[ 'exception' ])) {
            $message = $response[ 'message' ];
            if (isset($response[ 'debuginfo' ])) {
                $message .= ': ' . $response[ 'debuginfo' ];
            } else {
                $message .= '. Sometimes turning on debug mode might show the exact error';
            }

            throw new MoodleException($message);
        }

        return $response;
    }
}
