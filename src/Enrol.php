<?php

namespace Cadix\LaravelMoodle;

use Cadix\LaravelMoodle\Exception\MoodleException;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\ArrayShape;

class Enrol
{
    public function __construct(public Client $client)
    {
    }

    /**
     * Get enrolled users by course id
     *
     * @param int $course_id
     * @return array|null
     * @throws GuzzleException|MoodleException
     */
    public function core_enrol_get_enrolled_users(int $course_id): array|null
    {
        $this->client->url = 'core_enrol_get_enrolled_users';
        $this->client->url .= '&courseid=' . $course_id;

        return $this->client->request();
    }

    /**
     * Get the list of courses where a user is enrolled in
     *
     * @param int $user_id
     * @return array|null
     * @throws GuzzleException|MoodleException
     */
    public function core_enrol_get_users_courses(int $user_id): array|null
    {
        $this->client->url = 'core_enrol_get_users_courses';
        $this->client->url .= '&userid=' . $user_id;

        return $this->client->request();
    }

    /**
     * Manual enrol users
     *
     * @param array $enrolments
     * @return bool
     * @throws GuzzleException
     * @throws MoodleException
     */
    public function enrol_manual_enrol_users(
        #[ArrayShape([
            [
                'roleid'    => 'int',
                'userid'    => 'int',
                'courseid'  => 'int',
                'timestart' => 'int|null',
                'timeend'   => 'int|null',
                'suspend'   => 'int|null',
            ],
        ])]
        array $enrolments
    ): bool {
        $this->client->url = 'enrol_manual_enrol_users';

        $required_columns = ['roleid', 'userid', 'courseid'];

        foreach ($enrolments as $enrolment => $columns) {
            $provided_columns = array_keys($columns);
            $differences = array_diff($required_columns, $provided_columns);
            foreach ($differences as $difference) {
                if (in_array($difference, $required_columns, true)) {
                    throw new MoodleException('Required field '.$difference.' missing for enrolment '.$enrolment);
                }
            }

            foreach ($columns as $column => $value) {
                $this->client->url .= '&enrolments[' . $enrolment . '][' . $column . ']=' . $value;
            }
        }

        return is_null($this->client->request());
    }

    public function create(
        #[ArrayShape([
            'roleid'    => 'int',
            'userid'    => 'int',
            'courseid'  => 'int',
            'timestart' => 'int|null',
            'timeend'   => 'int|null',
            'suspend'   => 'int|null',
        ])]
        array $enrolment
    ): bool {
        return $this->enrol_manual_enrol_users([$enrolment]);
    }

    public function createMany(
        #[ArrayShape([
            [
                'roleid'    => 'int',
                'userid'    => 'int',
                'courseid'  => 'int',
                'timestart' => 'int|null',
                'timeend'   => 'int|null',
                'suspend'   => 'int|null',
            ],
        ])]
        array $enrolments
    ): bool {
        return $this->enrol_manual_enrol_users($enrolments);
    }
}
