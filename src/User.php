<?php

namespace Cadix\LaravelMoodle;

use Cadix\LaravelMoodle\Exception\MoodleException;
use Cadix\LaravelMoodle\Facades\Enrol;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\ExpectedValues;

class User
{
    public function __construct(public Client $client)
    {
    }

    /**
     * Create users
     *
     * @param array $users
     *
     * @return array|null
     * @throws GuzzleException
     * @throws Exception
     */
    public function core_user_create_users(array $users): array|null
    {
        $this->client->url = 'core_user_create_users';

        foreach ($users as $user => $columns) {
            foreach ($columns as $column => $value) {
                $this->client->url .= '&users[' . $user . '][' . $column . ']=' . $value;
            }
        }

        return $this->client->request();
    }

    /**
     * @param array $user
     * @return array|null
     * @throws GuzzleException
     */
    public function create(
        #[ArrayShape([
            'createpassword'    => 'int',
            'username'          => 'string',
            'auth'              => 'string',
            'password'          => 'string|null',
            'firstname'         => 'string',
            'lastname'          => 'string',
            'email'             => 'string',
            'maildisplay'       => 'int|null',
            'city'              => 'string|null',
            'country'           => 'string|null',
            'timezone'          => 'string|null',
            'description'       => 'string|null',
            'firstnamephonetic' => 'string|null',
            'lastnamephonetic'  => 'string|null',
            'middlename'        => 'string|null',
            'alternatename'     => 'string|null',
            'interests'         => 'string|null',
            'idnumber'          => 'string',
            'institution'       => 'string|null',
            'department'        => 'string|null',
            'phone1'            => 'string|null',
            'phone2'            => 'string|null',
            'address'           => 'string|null',
            'lang'              => 'string|null',
            'calendartype'      => 'string|null',
            'theme'             => 'string|null',
            'mailformat'        => 'int|null',
            'customfields'      =>
                [
                    [
                        'type'  => 'string|null',
                        'value' => 'string|null',
                    ],
                ],
            'preferences'       =>
                [
                    [
                        'type'  => 'string|null',
                        'value' => 'string|null',
                    ],
                ],
        ])]
        array $user
    ): array|null {
        return $this->core_user_create_users([$user])[ 0 ];
    }

    /**
     * @throws GuzzleException
     */
    public function createMany(array $users): array|null
    {
        return $this->core_user_create_users($users);
    }

    /**
     * Delete users - admin function
     * ^v2.0
     *
     * @param array $users
     *
     * @return bool
     * @throws GuzzleException|MoodleException
     */
    public function core_user_delete_users(array $users): bool
    {
        $this->client->url = 'core_user_delete_users';

        $total_users = count($users);
        for ($i = 0; $i < $total_users; $i++) {
            if (! is_int($users[ $i ])) {
                continue;
            }

            $this->client->url .= '&userids[' . $i . ']=' . $users[ $i ];
        }

        return is_null($this->client->request());
    }

    public function delete(array|int $users): bool
    {
        if (! is_array($users) && is_int($users)) {
            $users = [$users];
        }

        return $this->core_user_delete_users($users);
    }

    /**
     * Check if a user exists
     *
     * @param array $field
     *
     * @return bool
     * @throws GuzzleException
     * @throws Exception
     */
    public function core_user_exists(
        #[ArrayShape([
            'email'    => 'string|null',
            'username' => 'string|null',
        ])]
        array $field
    ): bool {
        // If more than one field is submitted
        if (count($field) > 1) {
            throw new MoodleException('Can only check if user exists based on 1 field');
        }

        $key = array_keys($field)[ 0 ];
        $value = $field[ $key ];
        $allowedFields = ['email', 'username'];

        if (! in_array($key, $allowedFields, false)) {
            throw new MoodleException('Can only use one of the following fields ' . implode(', ', $allowedFields) . ' to check if user exists');
        }

        $this->client->url = 'core_user_get_users_by_field';
        $this->client->url .= '&field=' . $key;
        $this->client->url .= '&values[0]=' . $value;

        $result = $this->client->request();

        return is_countable($result) && count($result) > 0;
    }

    public function exists(array $field): bool
    {
        return $this->core_user_exists($field);
    }

    /**
     * Acts the same as Laravel's firstOrCreate method
     *
     * @param string $field
     * @param array $user
     *
     * @return array
     * @throws GuzzleException
     */
    public function firstOrCreate(string $field, array $user): array
    {
        if (! $this->core_user_exists([$field => $user[ $field ]])) {
            $this->create($user);
        }

        return $this->find($user[ $field ], $field);
    }

    /**
     * Search for users matching the parameters
     *
     * @param array $fields
     *
     * @return array|null
     * @throws GuzzleException|MoodleException
     */
    public function core_user_get_users(
        #[ArrayShape([
            'id'        => 'int|null',
            'lastname'  => 'string|null',
            'firstname' => 'string|null',
            'idnumber'  => 'string|null',
            'username'  => 'string|null',
            'email'     => 'string|null',
            'auth'      => 'string|null',
        ])]
        array $fields
    ): array|null {
        $this->client->url = 'core_user_get_users';

        if ($fields) {
            $total_fields = count($fields);
            for ($i = 0; $i < $total_fields; $i++) {
                $key = array_keys($fields)[ 0 ];

                if (! in_array($key, ['id', 'lastname', 'firstname', 'idnumber', 'username', 'email', 'auth'])) {
                    return false;
                }

                $this->client->url .= '&criteria[' . $i . '][key]=' . $key;
                $this->client->url .= '&criteria[' . $i . '][value]=' . $fields[ $key ];
            }
        }

        return $this->client->request()[ 'users' ];
    }

    /**
     * @throws GuzzleException
     */
    public function all(array $fields): array|null
    {
        return $this->core_user_get_users($fields);
    }

    /**
     * Retrieve users' information for a specified unique field
     * If you want to do a user search, use core_user_get_users()
     *
     * @param array $fields
     *
     * @return object|null
     * @throws GuzzleException
     * @throws Exception
     */
    public function core_user_get_users_by_field(
        #[ArrayShape([
            'id'       => 'int|null',
            'idnumber' => 'string|null',
            'username' => 'string|null',
            'email'    => 'string|null',
        ])]
        array $fields
    ): array|null {
        if (count($fields) > 1) {
            throw new MoodleException('Only one field field can be used');
        }

        $field = array_keys($fields)[ 0 ];
        $value = $fields[ $field ];
        $allowed_fields = ['id', 'idnumber', 'username', 'email'];

        if (! in_array($field, $allowed_fields, false)) {
            throw new MoodleException('Key "' . $field . '" not allowed, must be one of: ' . implode(', ', $allowed_fields));
        }

        $this->client->url = 'core_user_get_users_by_field';
        $this->client->url .= '&field=' . $field;
        $this->client->url .= '&values[0]=' . $value;

        $results = $this->client->request();

        if (! is_countable($results) || count($results) === 0) {
            return null;
        }

        return $results;
    }

    /**
     * @throws GuzzleException
     */
    public function find(
        string|int $value,
        #[ExpectedValues([
            'id',
            'idnumber',
            'username',
            'email',
        ])]
        string $field = 'id'
    ): array|null {
        $found = $this->core_user_get_users_by_field([$field => $value]);
        if (! $found) {
            return $found;
        }

        return $found[ 0 ];
    }

    /**
     * Update specific user
     *
     * @param array $users
     *
     * @return bool
     * @throws GuzzleException|MoodleException
     */
    public function core_user_update_users(array $users): bool
    {
        $this->client->url = 'core_user_update_users';

        foreach ($users as $user => $columns) {
            foreach ($columns as $column => $value) {
                $this->client->url .= '&users[' . $user . '][' . $column . ']=' . $value;
            }
        }

        return is_null($this->client->request());
    }

    public function update(
        #[ArrayShape([
            'id'                => 'int',
            'username'          => 'string|null',
            'auth'              => 'string|null',
            'suspended'         => 'int|null',
            'password'          => 'string|null',
            'firstname'         => 'string|null',
            'lastname'          => 'string|null',
            'email'             => 'string|null',
            'maildisplay'       => 'int|null',
            'city'              => 'string|null',
            'country'           => 'string|null',
            'timezone'          => 'string|null',
            'description'       => 'string|null',
            'userpicture'       => 'int|null',
            'firstnamephonetic' => 'string|null',
            'lastnamephonetic'  => 'string|null',
            'middlename'        => 'string|null',
            'alternatename'     => 'string|null',
            'interests'         => 'string|null',
            'idnumber'          => 'string|null',
            'institution'       => 'string|null',
            'department'        => 'string|null',
            'phone1'            => 'string|null',
            'phone2'            => 'string|null',
            'address'           => 'string|null',
            'lang'              => 'string|null',
            'calendartype'      => 'string|null',
            'theme'             => 'string|null',
            'mailformat'        => 'int|null',
            'customfields'      => [
                [
                    'type'  => 'string|null',
                    'value' => 'string|null',
                ],
            ],
            'preferences'       =>
                [
                    [
                        'type'  => 'string|null',
                        'value' => 'string|null',
                    ],
                ],
        ])]
        array $user
    ): bool {
        return $this->core_user_update_users([$user]);
    }

    /**
     * Get the list of courses where a user is enrolled in              \
     *
     * @param int $user_id
     * @return array|null
     */
    public function courses(int $user_id): array|null
    {
        return Enrol::core_enrol_get_users_courses($user_id);
    }
}
