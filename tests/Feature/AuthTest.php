<?php

namespace Cadix\LaravelMoodle\Tests\Feature;

use Cadix\LaravelMoodle\Facades\Auth;
use Cadix\LaravelMoodle\Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class AuthTest extends TestCase
{
    use WithFaker;

    /*public function test_it_can_create_a_user(): void
    {
        $data = [
            'username'  => $this->faker->userName . $this->faker->randomNumber(),
            'password'  => Str::random() . 'Ap0!',
            'firstname' => $this->faker->firstName,
            'lastname'  => $this->faker->lastName,
            'email'     => $this->faker->safeEmail,
        ];

        $user = Auth::create( $data );
        dd( $user );
    }*/

    public function test_it_can_send_a_password_reset_email(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(file_get_contents( __DIR__ . '/../_responses/auth/password_reset.json' ), 200, [ 'Content-Type' => 'application/json']),
        ]);

        $username = 'cthompson19';
        $response = Auth::core_auth_request_password_reset('username', $username);

        $url = config('moodle.uri').
            'core_auth_request_password_reset&username='.$username;

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsBool($response);
        $this->assertTrue($response);
    }

    /*public function test_it_can_send_a_confirmation_email(): void
    {
        $response = Auth::core_auth_resend_confirmation_email( 'muller.rahul3', 'Th!ssh0uld' );

        dd( $response );

        $this->assertIsBool( $response );
        $this->assertTrue( $response );
    }*/
}
