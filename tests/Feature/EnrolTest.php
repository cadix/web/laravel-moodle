<?php

namespace Cadix\LaravelMoodle\Tests\Feature;

use Cadix\LaravelMoodle\Facades\Enrol;
use Cadix\LaravelMoodle\Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;

class EnrolTest extends TestCase
{
    public function test_it_can_create_single_enrolment(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $data = [
            'roleid'   => 5, // default for student
            'userid'   => 6,
            'courseid' => 4,
        ];

        $enrolled = Enrol::create($data);

        $url = config('moodle.uri').
            'enrol_manual_enrol_users&enrolments%5B0%5D%5Broleid%5D=' .
            $data['roleid']
            .'&enrolments%5B0%5D%5Buserid%5D='.
            $data['userid']
            .'&enrolments%5B0%5D%5Bcourseid%5D='.
            $data['courseid'];

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsBool($enrolled);
        $this->assertTrue($enrolled);
    }

    public function test_it_can_create_enrolment_for_limited_time(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $data = [
            'roleid'    => 5, // default for student
            'userid'    => 6,
            'courseid'  => 4,
            'timestart' => Carbon::now()->timestamp,
            'timeend'   => Carbon::now()->addWeek()->timestamp,
        ];

        $enrolled = Enrol::create($data);

        $url = config('moodle.uri').
            'enrol_manual_enrol_users&enrolments%5B0%5D%5Broleid%5D=' .$data['roleid'].
            '&enrolments%5B0%5D%5Buserid%5D='.$data['userid'].
            '&enrolments%5B0%5D%5Bcourseid%5D='.$data['courseid'].
            '&enrolments%5B0%5D%5Btimestart%5D='. $data['timestart'].
            '&enrolments%5B0%5D%5Btimeend%5D='.$data['timeend'];

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsBool($enrolled);
        $this->assertTrue($enrolled);
    }

    public function test_it_can_create_multiple_enrolments(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $data = [
            [
                'roleid'   => 5, // default for student
                'userid'   => 6,
                'courseid' => 4,
            ],
            [
                'roleid'   => 5, // default for student
                'userid'   => 8,
                'courseid' => 4,
            ],
        ];

        $enrolled = Enrol::createMany($data);

        $url = config('moodle.uri').
            'enrol_manual_enrol_users&enrolments%5B0%5D%5Broleid%5D=' .
            $data[0]['roleid']
            .'&enrolments%5B0%5D%5Buserid%5D='.
            $data[0]['userid']
            .'&enrolments%5B0%5D%5Bcourseid%5D='.
            $data[0]['courseid']
        .'&enrolments%5B1%5D%5Broleid%5D=' .
            $data[1]['roleid']
            .'&enrolments%5B1%5D%5Buserid%5D='.
            $data[1]['userid']
            .'&enrolments%5B1%5D%5Bcourseid%5D='.
            $data[1]['courseid'];

        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url &&
                $request->method() === 'POST';
        });

        $this->assertIsBool($enrolled);
        $this->assertTrue($enrolled);
    }

    public function test_on_enroll_it_will_give_an_error_when_a_required_field_is_missing(): void
    {
        Http::fake([
            config('moodle.host').'/*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $data = [
            [
                'roleid'   => 5, // default for student
                'userid'   => 6,
                'courseid' => 4,
            ],
            [
                // missing roleid
                'userid'    => 8,
                'courseid'  => 4,
                'timestart' => strtotime(Carbon::now()->toDateTimeString()),
            ],
        ];

        $this->expectException(\Cadix\LaravelMoodle\Exception\MoodleException::class);
        $this->expectExceptionMessage('Required field roleid missing for enrolment 1');

        $enrolled = Enrol::createMany($data);
    }
}
